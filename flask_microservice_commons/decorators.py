from connexion.exceptions import OAuthScopeProblem


def role_required(role: str):
    """
    To use as a decorator for connexion_ API resources. Raises an :class:`OAuthScopeProblem` if the given
    role is not in the ``roles`` value in the ``user`` object.

    Example::

        @role_required("admin")
        def amdin_only_resource(user, token_info):
            return {"msg": "For admins only!"}, 200

    .. _connexion: https://github.com/zalando/connexion

    :param role: (str) A string containing the role required
    :raises: :class:`connexion.exceptions.OAuthScopeProblem`
    """

    def wrap(f):
        def wrapped_f(user, token_info, **kwargs):
            roles = user.get("roles", [])

            if role not in roles:
                raise OAuthScopeProblem(
                    description="Provided user doesn't have the required access rights",
                    required_scopes=role,
                    token_scopes=roles,
                )

            return f(user, **kwargs)

        return wrapped_f

    return wrap
