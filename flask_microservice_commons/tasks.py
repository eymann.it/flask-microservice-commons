import time
import logging
from flask import current_app


async def handle_session_revoked(msg, **kwargs):
    """
    Task: Handle auth.session_revoked
    Append revoked user to cache
    """
    logging.debug(f"[api.tasks] Handling auth.session_revoked for message {msg}")
    cache = current_app.extensions["microservice_commons"].cache
    timeout_at = msg["timestamp"] + current_app.config["JWT_LIFETIME_SECONDS"]
    timeout = timeout_at - int(time.time())
    if timeout > 0:
        cache.set(
            "revoked_user_{}".format(msg["user_guid"]), timeout_at, timeout=timeout
        )


async def handle_jwt_public_keys(msg, **kwargs):
    """
    Task: auth.jwt_public_keys
    Receives a new public key and writes it to the cache
    """
    logging.debug(
        f"[api.tasks] Received fresh public key from auth. Key id is {msg['key_id']}"
    )
    cache = current_app.extensions["microservice_commons"].cache

    # We have to postpone key deletion to the expiry time of the key
    # plus the lifetime of the JWT tokens so we cover clients that
    # got a token short before key expriy
    timeout = (
        msg["timestamp"]
        + current_app.config["JWT_PUBLIC_KEY_EXPIRES_AFTER"]
        + current_app.config["JWT_LIFETIME_SECONDS"]
        - int(time.time())
    )
    cache.add(
        "jwt_public_key_{}".format(msg["key_id"]), msg["public_key"], timeout=timeout
    )
    logging.debug(
        f"New key written into jwt_public_key_{msg['key_id']} (timeout in {timeout} seconds)"
    )
