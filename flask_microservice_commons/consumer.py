import asyncio
import json
import logging
import importlib
import inspect
from concurrent.futures.thread import ThreadPoolExecutor

from nats.aio.client import Client as NATS
from nats.aio.client import Msg as NatsMessage
from nats.aio import errors as nats_errors
from stan.aio.client import Client as STAN
from stan.aio.client import Msg as StanMessage

from flask import current_app


def handle_results(results: iter):
    for result in results:
        if isinstance(result, Exception):
            logging.error(f"[worker.consumer] Handling general error: {result}")


async def handle_message(
    channel: str, msg: dict, sc: STAN = None, s_msg: StanMessage = None
):
    event = asyncio.Event()
    # handle message
    tasks = current_app.extensions["microservice_commons"].tasks.get(channel, [])
    handler_path = ""
    handlers = []
    if not isinstance(tasks, (list, tuple)):
        tasks = [tasks]
    for task_definition in tasks:
        try:
            handler_path = task_definition.get("callback")
            m = importlib.import_module(".".join(handler_path.split(".")[:-1]))
            f = getattr(m, handler_path.split(".")[-1])
            if inspect.iscoroutinefunction(f):
                handlers.append(f(msg))
            else:
                loop = asyncio.get_running_loop()
                executor = ThreadPoolExecutor()
                asyncio.ensure_future(
                    loop.run_in_executor(executor, f, current_app._get_current_object())
                )
                logging.debug(
                    f"[worker.consumer] non async function for channel {channel}"
                )
        except ImportError:
            logging.error(
                f"[worker.consumer] Problem loading handler {handler_path} for channel {channel}"
            )
        except AttributeError:
            logging.warning(
                f"[worker.consumer] No handler for channel {channel} found at {handler_path}"
            )
    try:
        if handlers:
            results = await asyncio.gather(*handlers, return_exceptions=True)
            handle_results(results)
    finally:
        if sc and s_msg:
            await sc.ack(s_msg)
        event.set()


async def consume(
    nc: NATS, sc: STAN, run_once: bool = False, queue: asyncio.Queue = None
):
    consumer_queue = queue if queue else asyncio.Queue()

    async def callback(msg):
        nonlocal consumer_queue
        await consumer_queue.put(msg)

    # add session revoked task
    tasks = current_app.extensions["microservice_commons"].tasks
    for channel in tasks.keys():
        try:
            if tasks[channel]["durable"]:
                await sc.subscribe(
                    channel,
                    queue=current_app.config["NATS_QUEUE_GROUP"],
                    durable_name=current_app.config["NATS_QUEUE_GROUP"],
                    cb=callback,
                    manual_acks=True,
                )
            else:
                await nc.subscribe(
                    channel, queue=current_app.config["NATS_QUEUE_GROUP"], cb=callback
                )
        except nats_errors.NatsError as e:
            logging.error(
                f"[worker.consumer] Got an error from NATS trying to subscribe to channel '{channel}'"
            )
            raise e
        logging.info(f"[worker.consumer] Subscribed to channel {channel}")

    if not len(tasks.keys()):
        logging.warning(f"[worker.conumer] No subscriptions")
        return

    cl = consumer_loop(consumer_queue, sc, run_once)
    await asyncio.gather(cl, return_exceptions=False)


async def consumer_loop(
    consumer_queue: asyncio.Queue, sc: STAN, run_once: bool = False
):
    go = True
    while go:
        msg = await consumer_queue.get()
        stan_msg = None
        if isinstance(msg, NatsMessage):
            subject = msg.subject
        elif isinstance(msg, StanMessage):
            subject = msg.sub.subject
            stan_msg = msg
        else:
            try:
                subject = msg.subject
            except AttributeError:
                raise RuntimeError(f"Unkown message type {msg.__class__}")
        data = json.loads(msg.data.decode())
        logging.debug(f"[worker.consumer] Received {data['id']} on channel {subject}")
        asyncio.create_task(handle_message(subject, data, sc=sc, s_msg=stan_msg))
        if run_once:
            go = False
