from flask_microservice_commons import db


def create_event(channel: str, message: dict, durable: bool = False):
    event = EventOutbox(channel=channel, message=message, durable=durable)
    db.session.add(event)
    return event


class EventOutbox(db.Model):
    __tablename__ = "event_outbox"
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    channel = db.Column(db.String(128), nullable=False)
    message = db.Column(db.JSON, nullable=True)
    durable = db.Column(db.Boolean, default=False)
    status = db.Column(db.String(8), index=True)

    def __init__(self, channel: str, message: dict, durable: bool = False):
        self.channel = channel
        self.durable = durable
        self.message = message
        self.status = "pending"
