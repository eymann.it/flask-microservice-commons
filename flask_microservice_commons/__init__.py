import importlib
import uuid
from flask import Flask
from flask_caching import Cache
from flask_microservice_commons import worker
from flask_sqlalchemy import SQLAlchemy
from croniter import croniter

db = SQLAlchemy()
cache = Cache()


class ConfigurationError(Exception):
    pass


class Commons(object):
    def __init__(
        self,
        app: Flask = None,
        cache: Cache = None,
        local_cache: Cache = None,
        database: SQLAlchemy = None,
    ):
        global db
        self.app = app
        self.cache = cache
        self.local_cache = local_cache
        if database:
            db = database
        self.db = db
        if app is not None:
            self.init_app(app, cache, local_cache, db)

    def init_app(
        self,
        app: Flask,
        cache: Cache = None,
        local_cache: Cache = None,
        database: SQLAlchemy = None,
    ):
        # global db
        self.cache = cache or self.cache
        self.local_cache = local_cache or self.local_cache
        self.db = database or self.db
        app.config.setdefault("SSH_KEY_FILE", None)
        app.config.setdefault("ASYNC_TASKS", [])
        app.config.setdefault("CRONJOBS", [])
        app.config.setdefault("NATS_SERVER", "localhost:4222")
        app.config.setdefault("JWT_LIFETIME_SECONDS", 500)
        app.config.setdefault("NATS_QUEUE_GROUP", None)
        app.config.setdefault("JWT_PUBLIC_KEY_EXPIRES_AFTER", 3600)
        app.config.setdefault(
            "STAN_CLIENT_ID",
            f"{app.config.get('NATS_QUEUE_GROUP')}-{uuid.uuid4().hex.upper()[:5]}",
        )

        if not hasattr(app, "extensions"):
            app.extensions = {}
        app.teardown_appcontext(self.teardown)
        self.app = app
        self.app.extensions["microservice_commons"] = _CommonsConfig(
            self, self.cache, self.local_cache, self.db
        )

    def teardown(self, exception):
        pass
        # ctx = _app_ctx_stack.top
        # clean up if necessary

    def run_worker(self):
        worker.main()


class _CommonsConfig(object):
    def __init__(
        self, commons: Commons, cache: Cache, local_cache: Cache, database: SQLAlchemy
    ):
        self.commons = commons
        self.cache = cache
        self.local_cache = local_cache
        self.db = database

        try:
            assert isinstance(self.tasks, dict)
        except AssertionError:
            pass

    @property
    def tasks(self) -> dict:
        try:
            custom_tasks = dict(self.commons.app.config.get("ASYNC_TASKS"))
        except ValueError:
            raise ConfigurationError(
                f"Cannot parse task specifications from object {self.commons.app.config.get('ASYNC_TASKS')}"
            )

        tasks = {}
        for key in custom_tasks:
            task = custom_tasks[key]
            if not isinstance(task, dict):
                raise ConfigurationError(
                    f"Task definition has to be a dict, got {task.__class__} instead."
                )
            if "callback" not in task.keys():
                raise ConfigurationError(
                    f"No callback function declared in definition for task {key}: {task}"
                )
            task.setdefault("durable", False)
            task.setdefault("queue", None)
            tasks[key] = task
        tasks.setdefault(
            "auth.session_revoked",
            {
                "callback": "flask_microservice_commons.tasks.handle_session_revoked",
                "durable": False,
                "queue": self.commons.app.config["NATS_QUEUE_GROUP"],
            },
        )
        tasks.setdefault(
            "auth.jwt_public_keys",
            {
                "callback": "flask_microservice_commons.tasks.handle_jwt_public_keys",
                "durable": False,
                "queue": self.commons.app.config["NATS_QUEUE_GROUP"],
            },
        )
        return tasks

    @property
    def cronjobs(self) -> iter:  # noqa: C901
        try:
            jobs = tuple(self.commons.app.config.get("CRONJOBS"))
        except ValueError:
            raise ConfigurationError(
                f"Cannot parse cronjob specifications from object {self.commons.app.config.get('CRONJOBS')}"
            )
        for job in jobs:
            errors = []
            func = None
            args = ()
            spec = job[0]
            if not croniter.is_valid(spec):
                errors.append("spec")

            handler_path = job[1][0]
            try:
                m = importlib.import_module(".".join(handler_path.split(".")[:-1]))
                func = getattr(m, handler_path.split(".")[-1])
            except ImportError:
                errors.append("path")

            if len(job[1]) > 1:
                try:
                    args = tuple(job[1][1])
                except ValueError:
                    errors.append("args")

            if errors:
                raise ConfigurationError(
                    f"Cannot parse specification for cronjob: {job} {errors}"
                )
            yield {"spec": spec, "func": func, "args": args}
