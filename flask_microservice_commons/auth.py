import logging
import jwt
from connexion.exceptions import OAuthProblem
from flask import current_app
from jwt.algorithms import RSAAlgorithm

from flask_microservice_commons.tools import get_from_local_cache


class SSHKeyNotFound(Exception):
    pass


class SSHKeyError(Exception):
    pass


class TokenInvalidError(OAuthProblem):
    description = "The token used to access this resource is invalid"


class TokenExpiredError(OAuthProblem):
    description = "The token used to access this resource is expired"


class TokenRevokedError(OAuthProblem):
    description = "The token used to access this resource has been revoked"


def jwt_auth(token: str) -> dict:
    """securitySchemes/jwt"""
    return decode_token(token)


def decode_token(token: str, public_key: str = None) -> dict:  # noqa: C901
    """
    Decodes a JWT Token. The public SSH key is used to confirm the signature and can be passed
    as an argument (mostly for testing purposes). Normally the key published by th auth service will
    be used. We expect the JWT to carry ``key`` claim which contains the ID of the keypair used to sign
    the token. The auth service published the public keys in use together with their IDs.

    :param token: The plain JWT token
    :param public_key: Enforce public to verify the signature
    """
    if not public_key:
        # the public keys in use arrive as events from the auth service and are stored in the cache
        # to identify the id of the keypair used for this token is saved in the 'key' claim
        try:
            preliminary_data = jwt.decode(token, verify=False)
        except jwt.exceptions.DecodeError as exp:
            raise TokenInvalidError(f"malformed JWT token: {exp}")

        key_id = preliminary_data.get("key", None)
        if key_id:
            # retrieve key from cache
            public_key = get_from_local_cache(f"jwt_public_key_{key_id}")
        if not public_key:
            raise TokenInvalidError(
                f"SSH key file cannot be found in cache key jwt_public_key_{key_id}"
            )

    # verify public key
    rsa = RSAAlgorithm(RSAAlgorithm.SHA256)
    try:
        rsa.prepare_key(public_key)
    except ValueError:
        logging.error(f"[auth] Invalid public key: {public_key}")
        raise TokenInvalidError("Invalid public key")

    try:
        data = jwt.decode(token, public_key, algorithms=["RS256"])
    except jwt.ExpiredSignatureError:
        raise TokenExpiredError
    except jwt.InvalidAlgorithmError:
        # write a log entry whenever someone tries to use a token encoded with the wrong algorithm
        logging.warning(
            "[api.auth] Attempted authorization using a token encoded with the wrong algorithm"
        )
        raise TokenInvalidError
    except jwt.InvalidTokenError:
        # write a log entry whenever someone tries to use an invalid token
        logging.warning("[api.auth] Attempted authorization with an invalid token")
        raise TokenInvalidError

    # Check cache for revoked users
    if current_app.extensions["microservice_commons"].cache:
        revoke = current_app.extensions["microservice_commons"].cache.get(
            "revoked_user_{}".format(data["sub"]["guid"])
        )
    else:
        revoke = False

    if revoke:
        try:
            if int(revoke) >= data["exp"]:
                raise TokenExpiredError
        except ValueError:
            # junk data in cache
            logging.warning(
                f"[api.auth] Found junk data in revoked_user_{data['sub']['guid']}: {revoke}"
            )
    return data
