import asyncio
import aiopg
import logging
import json
from nats.aio.client import Client as NATS
from nats.aio import errors as nats_errors
from stan.aio.client import Client as STAN
from flask import current_app

from flask_microservice_commons.tools import execute_db_command

CHANNEL_NAME = "outgoing_event"


class Message(object):
    def __init__(self, id: int, channel: str, payload: str = ""):
        self.id = id
        self.channel = channel
        self.payload = payload

    def __str__(self):
        return self.id

    def set_payload(self, payload: dict):
        self.payload = payload

    def to_json(self):
        assert isinstance(self.payload, dict)
        ret = {"id": self.id}
        ret.update(self.payload)
        return ret


async def publish_message(
    nc: NATS, sc: STAN, pool: aiopg.pool, message: Message
) -> bool:
    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            success = False
            await execute_db_command(
                cur,
                f"BEGIN; "
                f"UPDATE event_outbox SET status='boarding' "
                f"WHERE id='{message.id}' AND status='pending' RETURNING message, durable;",
            )

            if cur.rowcount > 0:
                result = await cur.fetchone()
                message.set_payload(result[0])
                try:
                    if result[1] == 0:
                        await nc.publish(
                            message.channel, json.dumps(message.to_json()).encode()
                        )
                    else:
                        await sc.publish(
                            message.channel, json.dumps(message.to_json()).encode()
                        )
                    success = True
                except nats_errors.NatsError as e:
                    logging.error(
                        f"[worker.producer] Cannot publish message {message.id}: {e}"
                    )
                finally:
                    if success:
                        await execute_db_command(
                            cur,
                            f"DELETE FROM event_outbox WHERE id='{message.id}'; COMMIT;",
                        )
                        logging.info(
                            f"[worker.producer] Published {message.id} on channel {message.channel}"
                        )
                    else:
                        await execute_db_command(cur, "ROLLBACK;")
            else:
                # someone else sent the message, so we're good!
                success = True
            return success


async def dispatch_messages(
    pool: aiopg.pool, nc: NATS, sc: STAN, queue: asyncio.Queue, run_once: bool = False
):
    go = True
    while go:
        message = await queue.get()
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                if nc.is_connected and not nc.is_draining:
                    # assure this is the oldest message
                    await execute_db_command(
                        cur,
                        f"SELECT EXISTS(SELECT 1 FROM event_outbox WHERE id < {message.id})",
                    )
                    result = await cur.fetchone()
                    if not result[0]:
                        await publish_message(nc, sc, pool, message)
                    else:
                        logging.warning(
                            "[worker.producer] dispatcher: there are earlier rows still pending"
                        )
                else:
                    logging.warning(
                        f"[worker.producer] eventstore offline, message queued: {message}"
                    )
                if run_once:
                    go = False


async def listen(pool: aiopg.pool, queue: asyncio.Queue, run_once: bool = False):
    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            await execute_db_command(cur, f"LISTEN {CHANNEL_NAME}")
            go = True
            while go:
                response = await conn.notifies.get()
                try:
                    data = json.loads(response.payload)["data"]
                    msg = Message(id=data["id"], channel=data["channel"])
                    logging.debug(
                        f"[worker.producer] Message bound to be dispatched: {msg}"
                    )
                    await queue.put(msg)
                except AttributeError:
                    logging.warning(
                        f"[worker.producer] Response from LISTEN cannot be parsed, got: {response}"
                    )
                except (TypeError, json.decoder.JSONDecodeError):
                    logging.warning(
                        f"[worker.producer] Data from DB message malformed: {data}"
                    )
                if run_once:
                    go = False


async def collect_pending(nc: NATS, sc: STAN, pool: aiopg.pool):
    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            go = True
            while go:
                wait_for = 5  # normally we wait for 5 seconds
                logging.debug("[worker.producer] looking for pending messages...")
                if nc.is_connected and not nc.is_draining:
                    await execute_db_command(
                        cur,
                        f"SELECT id, channel, message FROM event_outbox WHERE status='pending' ORDER BY id;",
                    )
                    if cur.rowcount > 0:
                        logging.debug(
                            f"[worker.producer] {cur.rowcount} pending messages found"
                        )
                        async for row in cur:
                            success = await publish_message(
                                nc=nc,
                                sc=sc,
                                pool=pool,
                                message=Message(
                                    id=row[0], channel=row[1], payload=row[2]
                                ),
                            )
                            if row[2] == {"kill_process": True}:
                                go = False
                            if not success:
                                break
                        # after having found pending rows we better restart immediately
                        wait_for = 0
                await asyncio.sleep(wait_for)


async def producer(nc: NATS, sc: STAN):
    queue = asyncio.Queue()
    async with aiopg.create_pool(
        current_app.config["SQLALCHEMY_DATABASE_URI"], maxsize=10
    ) as pool:
        listener = listen(pool, queue)
        dispatcher = dispatch_messages(pool, nc, sc, queue)
        collector = collect_pending(nc, sc, pool)
        await asyncio.gather(listener, dispatcher, collector, return_exceptions=False)
