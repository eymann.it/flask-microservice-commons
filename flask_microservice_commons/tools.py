import os
import signal
import logging
import psycopg2
from flask import current_app


async def execute_db_command(cursor, command):
    try:
        return await cursor.execute(command)
    except psycopg2.Error as e:
        logging.error(
            f"[producer] caught error while executing sql command: {e.__class__}"
        )
        logging.error(str(e))
        os.kill(os.getpid(), signal.SIGINT)


def get_from_local_cache(key: str, update_if_missing: bool = True) -> bytes:
    """
    Tries to retrieve key from local cache. If not found the shared
    cache is accessed.
    """
    local_cache = current_app.extensions["microservice_commons"].local_cache
    cache = current_app.extensions["microservice_commons"].cache

    value = None
    if local_cache:
        value = local_cache.get(key)
        if not value:
            value = cache.get(key)
            if value and update_if_missing:
                local_cache.set(key, value)
    else:
        value = cache.get(key)

    return value


def request_public_keys():
    """Request public keys in use from auth service"""
    from flask_microservice_commons.models import create_event

    db = current_app.extensions["microservice_commons"].db
    create_event("auth.request_public_keys", message={})
    db.session.commit()
