import click
from flask.cli import with_appcontext


@click.command()
@with_appcontext
def worker():
    """Runs the worker process"""
    from flask_microservice_commons import worker

    worker.main()


@click.command()
@with_appcontext
def request_publickeys():
    from flask_microservice_commons import tools

    tools.request_public_keys()
