import asyncio
from concurrent.futures.thread import ThreadPoolExecutor

import aiocron
import logging
import signal
import functools

from nats.aio.client import Client as NATS
from nats.aio import errors as nats_errors
from stan.aio.client import Client as STAN
from flask import current_app

from flask_microservice_commons import consumer, producer


# for testing only
async def empty_coroutine():
    pass


def handle_exception(
    nc: NATS, sc: STAN, loop: asyncio.AbstractEventLoop, context: dict
):
    """
    Invoked whenever an otherwise unhandled exception occurs in one of the tasks present in the event loop.
    At this time any non handled exception is regarded as a breaking error thus a graceful shutdown in initiated.

    :param nc: NATS connection
    :param sc: STAN connection
    :param loop: Current asyncio loop
    :param context: Exception context
    """

    # context["message"] will always be there; but context["exception"] may not
    msg = context.get("exception", context["message"])
    logging.error(f"[worker] Caught exception: {msg}")
    logging.info("[worker] Shutting down...")
    asyncio.create_task(shutdown(loop, nc, sc))


def stan_lost_connection(*args, **kwargs):
    logging.error("[worker] Lost connection to NATS streaming")


async def run_as_thread(func, loop, *args):
    """
    Helper function used to start a synchronous task in a separate thread using ``ThreadPoolExecutor``

    :param func: Function to be executed synchronously
    :param loop: Current asyncio loop
    :param args: Position arguments will be passed over to ``func``
    """
    executor = ThreadPoolExecutor()
    new_args = (current_app._get_current_object(),) + args
    await loop.run_in_executor(executor, func, *new_args)


async def setup(loop: asyncio.AbstractEventLoop, nc: NATS, sc: STAN):
    """
    Open external connections to the message broker and start services:

    * Message consumer
    * Message producer
    * Cronjobs defined in the ``CRONJOBS`` setting

    :param loop: Current asyncio loop
    :param nc: NATS connection
    :param sc: STAN connection
    """
    logging.info(
        "[worker] Connecting to NATS on {}...".format(
            current_app.config.get("NATS_SERVER")
        )
    )
    await nc.connect(current_app.config.get("NATS_SERVER"), io_loop=loop)
    logging.info(f"[worker] Connected to {nc.connected_url.netloc}")

    # Start session with NATS streaming
    logging.info(
        "[worker] Connecting to NATS STREAMING on {}...".format(
            current_app.config.get("NATS_SERVER")
        )
    )
    cluster_id = current_app.config.get("NATS_CLUSTER_ID")
    client_id = current_app.config.get("STAN_CLIENT_ID")
    conn_info = await sc.connect(
        cluster_id, client_id, nats=nc, conn_lost_cb=stan_lost_connection, loop=loop
    )
    logging.info(
        f"[worker] Connected to {cluster_id} as {client_id} with info {conn_info}"
    )

    # start producer and consumer worker
    loop.create_task(producer.producer(nc, sc))
    if len(current_app.extensions["microservice_commons"].tasks.keys()):
        loop.create_task(consumer.consume(nc, sc))

    # setting up cronjobs
    for job_spec in current_app.extensions["microservice_commons"].cronjobs:
        if asyncio.iscoroutinefunction(job_spec["func"]):
            aiocron.crontab(
                job_spec["spec"],
                func=job_spec["func"],
                args=job_spec["args"],
                start=True,
                loop=loop,
            )
        else:
            # start synchronous job in a thread
            aiocron.crontab(
                job_spec["spec"],
                func=run_as_thread,
                args=(job_spec["func"], loop) + job_spec["args"],
                start=True,
                loop=loop,
            )


async def shutdown(
    loop: asyncio.AbstractEventLoop, nc: NATS, sc: STAN, signal: int = None
):
    """
    This is the handler invoked if the according signal is received or in case of
    a non handled exception intercepted by ``handle_exception``. Stops the worker
    gracefully by canceling all running tasks and closing the connection to NATS.

    :param loop: Current async loop
    :param nc: NATS connection
    :param sc: STAN connection
    :param signal: Signal received
    """
    if signal:
        logging.info(f"[worker] Received exit signal {signal}")
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]

    [task.cancel() for task in tasks]

    logging.info(f"[worker] Cancelling {len(tasks)} outstanding tasks...")
    await asyncio.gather(*tasks, return_exceptions=True)
    if nc.is_connected:
        logging.info("[worker] Disconnecting NATS STREAMING...")
        try:
            await sc.close()
        except nats_errors.ErrTimeout:
            logging.error("[worker] Timeout from STAN while disconnecting")

        logging.info("[worker] Disconnecting NATS...")
        await nc.close()
        loop.stop()


def main(nc: NATS = None):
    """
    Setups and starts the main worker loop. The following signals are intercepted in order to shutdown
    the worker gracefully:

    * ``SIGHUP``
    * ``SIGTERM``
    * ``SIGINT``
    * ``SIGQUIT``

    :param nc: A not yet connected NATS instance (*default=None*)
    """
    if not current_app.config.get("NATS_SERVER"):
        from flask_microservice_commons import ConfigurationError

        raise ConfigurationError("Missing setting NATS_SERVER")

    # create event loop
    loop = asyncio.get_event_loop()

    if not nc:
        nc = NATS()
    sc = STAN()

    # interruption handling
    # signals we want to catch
    signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT, signal.SIGQUIT)
    for s in signals:
        loop.add_signal_handler(
            s, lambda s=s: asyncio.create_task(shutdown(loop, nc, sc, signal=s))
        )
    # noinspection PyTypeChecker
    loop.set_exception_handler(functools.partial(handle_exception, nc, sc))

    try:
        loop.create_task(setup(loop, nc=nc, sc=sc))
        loop.run_forever()
    finally:
        loop.close()
        logging.info("[worker] Successfully shutdown the service")
