.. Flask Microservice Commons documentation master file, created by
   sphinx-quickstart on Sat Sep  7 12:51:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Flask Microservice Commons documentation!
======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Worker
======
.. automodule:: flask_microservice_commons.worker
   :members: main, setup, shutdown, run_as_thread, handle_exception

Auth
====
.. automodule:: flask_microservice_commons.auth
   :members: decode_token

Decorators
==========
.. automodule:: flask_microservice_commons.decorators
   :members: role_required

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
