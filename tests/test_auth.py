import time
import jwt
from flask_caching import Cache
from flask_testing import TestCase
from flask_microservice_commons.auth import (
    decode_token,
    TokenInvalidError,
    TokenExpiredError,
    SSHKeyNotFound,
    jwt_auth,
)
from flask import Flask

from flask_microservice_commons import Commons

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend


class TestJWTAuthentication(TestCase):
    def create_app(self):
        app = Flask(__name__)
        app.debug = True
        app.env = "testing"
        app.testing = True
        self.cache = Cache(app, config={"CACHE_TYPE": "simple"})
        Commons(app, self.cache)
        return app

    def setUp(self) -> None:
        key = rsa.generate_private_key(
            backend=default_backend(), public_exponent=65537, key_size=2048
        )
        self.private_key = key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.PKCS8,
            serialization.NoEncryption(),
        )
        self.public_key = key.public_key().public_bytes(
            serialization.Encoding.OpenSSH, serialization.PublicFormat.OpenSSH
        )

    def tearDown(self) -> None:
        self.private_key = None
        self.public_key = None
        self.cache = None

    def test_decode_token(self):
        """Ensure decode_token function behaves correctly."""
        timestamp = int(time.time())
        claims = {
            "iss": "com.example",
            "iat": int(timestamp),
            "exp": int(timestamp + 60),
            "sub": {"guid": 1},
            "key": 1,
        }
        self.cache.set("jwt_public_key_1", self.public_key.decode("utf-8"))
        token = jwt.encode(claims, self.private_key, algorithm="RS256").decode("utf-8")
        payload = jwt_auth(token)

        self.assertIn("sub", payload.keys())
        self.assertIn("guid", payload["sub"].keys())
        self.assertEqual(claims["sub"]["guid"], payload["sub"]["guid"])

    def test_decode_error_is_handled(self):
        """Ensure DecodeToken errors fail gracefully"""
        # When we fetch the preliminary data in decode_token()
        # and a malformed token is submitted a jwt.exceptions.DecodeError
        # has to be catched and a TokenInvalid Exception is expected

        malformed_token = "just a string with random stuff"
        with self.assertRaises(TokenInvalidError):
            decode_token(malformed_token)

    def test_decode_token_with_invalid_signature(self):
        """Ensure log warning is created if invalid token is used"""
        claims = {"user_id": 1}
        token = jwt.encode(claims, self.private_key, algorithm="RS256").decode("utf-8")
        false_public_key = (
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDkXnnee81jVGG6468SOwmgblXl2gZ+fWgefGPGNHXBpSwfyss6vBR"
            "aJII/APpJ9IpSQkOKXsOz1ff8momjstV+TN9v5aj4/io4WE8gnQu9mj20Xgttn5cybKd5tkA/sNyrDMJl8Dr5+WpUeJ"
            "hwnS7EPE7Qr2qPLOm6GmDM4d938gU5YywQqJkDFU9O7N/xtka5vhxJo+gexeCvz5dr9453UPEdsIO1PXMzddfu7jYRm"
            "BsgCWo8zuRCUFATpr/HGq3hK7xs1AvLNxfHq9i8tFlaBggfqS+KNIxI849/t9h3Kzmit1ETU1ncrTpN3/piwHAuMkj3"
            "R9HeIG5v8BediEuz peter@peter-ThinkPad"
        )

        with self.assertRaises(TokenInvalidError):
            with self.assertLogs("foo", level="WARNING") as cm:
                payload = decode_token(token, public_key=false_public_key)
                self.assertEqual(
                    cm.output,
                    ["WARNING:foo:Attempted authorization with an invalid token"],
                )

    def test_token_expires(self):
        """Ensure an expired token can not be verified"""
        import time

        timestamp = int(time.time())
        claims = {
            "iss": "com.example",
            "iat": timestamp - 60,
            "exp": timestamp - 1,
            "user_id": 1,
        }

        token = jwt.encode(claims, self.private_key, algorithm="RS256").decode("utf-8")
        with self.assertRaises(TokenExpiredError):
            payload = decode_token(token, public_key=self.public_key)

    def test_token_wrong_algorythm(self):
        """Ensure a token encoded with the wrong algorythm is being discarded"""
        timestamp = int(time.time())
        claims = {
            "iss": "com.example",
            "iat": int(timestamp),
            "exp": int(timestamp + 60),
            "user_id": 1,
        }

        token = jwt.encode(claims, self.private_key, algorithm="HS256").decode("utf-8")
        with self.assertRaises(TokenInvalidError):
            with self.assertLogs("foo", level="WARNING") as cm:
                payload = decode_token(token, public_key=self.public_key)
                self.assertEqual(
                    cm.output,
                    [
                        "WARNING:foo:Attempted authorization using a token encoded with the wrong algorythm"
                    ],
                )

    def test_ssh_key_not_found(self):
        """Ensure an error is thrown in ssh key is missing"""
        claims = {"user_id": 1}
        token = jwt.encode(claims, self.private_key, algorithm="RS256").decode("utf-8")
        self.app.config["SSH_KEY_FILE"] = "/tmp/i_dont_exist"
        with self.assertRaises(TokenInvalidError):
            decode_token(token)

    def test_revoke(self):
        """Ensure tokens from revoked users are denied"""
        timestamp = int(time.time())
        claims = {
            "iss": "com.example",
            "iat": int(timestamp),
            "exp": int(timestamp) + 60,
            "sub": {"guid": 1},
        }

        token = jwt.encode(claims, self.private_key, algorithm="RS256").decode("utf-8")
        to = int(time.time()) + 60
        self.cache.set("revoked_user_1", to)
        with self.assertRaises(TokenExpiredError):
            decode_token(token, public_key=self.public_key)

    def test_revoke_with_junk_data(self):
        """Ensure tokens from revoked users are denied"""
        timestamp = int(time.time())
        claims = {
            "iss": "com.example",
            "iat": int(timestamp),
            "exp": int(timestamp) + 60,
            "sub": {"guid": 1},
        }

        token = jwt.encode(claims, self.private_key, algorithm="RS256").decode("utf-8")
        self.cache.set("revoked_user_1", "some_bullshit")
        with self.assertLogs(level="WARNING") as cm:
            decode_token(token, public_key=self.public_key)
            self.assertEqual(
                cm.output,
                [
                    "WARNING:root:[api.auth] Found junk data in revoked_user_1: some_bullshit"
                ],
            )
