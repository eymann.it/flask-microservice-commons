import asyncio
import signal
from unittest import mock

import asynctest

from tests.base import AsyncMock, BaseAsyncTestCase

from flask_microservice_commons import worker, consumer, producer
from tests.test_producer import create_pool


class NATS(object):
    class ConnectedURL(object):
        netloc = "testhost"

    def __init__(self):
        self.connected_url = self.ConnectedURL()

    async def connect(self, *args, **kwargs):
        return

    async def close(self):
        return

    @property
    def is_connected(self):
        return True


class STAN(NATS):
    pass


async def async_job():
    pass


def sync_job():
    pass


class TestWorker(BaseAsyncTestCase):
    @mock.patch("asyncio.get_event_loop", new=mock.Mock(return_value=mock.Mock()))
    @mock.patch("flask_microservice_commons.worker.setup", new=mock.Mock())
    def test_main(self):
        """Ensure the main function works as expected"""
        with self.assertLogs(level="INFO") as cm:
            worker.main()
            self.assertEqual(len(cm.output), 1)
            self.assertIn("Successfully shutdown the service", cm.output[0])

    @mock.patch("asyncio.create_task", new=mock.Mock())
    @mock.patch("flask_microservice_commons.worker.shutdown", new=mock.Mock())
    def test_handle_exception(self):
        """Ensure handle_exception() behaves as expected"""
        nc = NATS()
        sc = STAN()
        context = {"message": "test"}
        with self.assertLogs(level="INFO") as cm:
            worker.handle_exception(nc, sc, self.loop, context=context)
            self.assertEqual(len(cm.output), 2)
            self.assertIn("Caught exception: test", cm.output[0])
            self.assertIn("Shutting down...", cm.output[1])

    @asynctest.patch("flask_microservice_commons.worker.producer.producer")
    @asynctest.patch("flask_microservice_commons.worker.consumer.consume")
    async def test_contabs(self, *args):
        """Ensure async and sync functions are called approprietly"""

        self.app.config["CRONJOBS"] = [
            ["* * * * *", ["flask_microservice_commons.worker.empty_coroutine"]],
            ["* * * * *", ["tests.test_worker.sync_job"]],
        ]
        loop = asyncio.get_event_loop()
        mock_nats = asynctest.Mock(NATS())

        with mock.patch(
            "flask_microservice_commons.worker.aiocron.crontab"
        ) as mock_crontab:
            await worker.setup(loop, mock_nats, mock_nats)
            print(mock_crontab.mock_calls)

            mock_crontab.assert_has_calls(
                calls=[
                    mock.call(
                        mock.ANY,
                        func=worker.run_as_thread,
                        args=mock.ANY,
                        start=True,
                        loop=mock.ANY,
                    ),
                    mock.call(
                        mock.ANY,
                        func=worker.empty_coroutine,
                        args=mock.ANY,
                        start=True,
                        loop=mock.ANY,
                    ),
                ],
                any_order=True,
            )

    def test_worker_setup(self):
        """Ensure the worker starts up normally"""
        self.app.config["ASYNC_TASKS"] = [["test", {"callback": "test"}]]

        loop = asyncio.get_event_loop()
        with mock.patch(
            "flask_microservice_commons.worker.producer.producer", new=AsyncMock()
        ) as mock_producer:
            with mock.patch(
                "flask_microservice_commons.worker.consumer.consume", new=AsyncMock()
            ) as mock_consumer:
                self.loop.run_until_complete(worker.setup(loop, NATS(), STAN()))
                mock_producer.mock.assert_called_once()
                mock_consumer.mock.assert_called_once()

    async def test_shutdown(self):
        """Ensure the shutdown() function works as expected"""

        async def neverending_task():
            while True:
                await asyncio.sleep(1)

        running_tasks = len(asyncio.all_tasks())
        loop = asyncio.get_event_loop()
        loop.create_task(neverending_task())
        self.assertGreaterEqual(len(asyncio.all_tasks()), running_tasks)
        await worker.shutdown(loop=loop, nc=NATS(), sc=STAN(), signal=signal.SIGINT)
        self.assertEqual(len(asyncio.all_tasks()), running_tasks)
