import asyncio
import json
from aiopg.utils import _ContextManager
from contextlib import asynccontextmanager
from unittest import mock
import asynctest
from asynctest import Mock as AioMock

from flask_microservice_commons import producer
from flask_microservice_commons.producer import collect_pending, Message

from tests.base import BaseAsyncTestCase, AsyncMock


class NATS:
    """fake nats"""

    def __init__(
        self,
        is_connected: bool = True,
        is_draining: bool = False,
        raises: Exception = None,
    ):
        self.is_connected = is_connected
        self.is_draining = is_draining
        self._raises = raises

    async def publish(self, *args, **kwargs):
        if self._raises:
            raise self._raises


class STAN(NATS):
    pass


def _run(coro):
    return asyncio.get_event_loop().run_until_complete(coro)


class MockNotify(object):
    def __init__(self):
        self.pid = 109
        self.channel = "outgoing_event"
        self.data = {"id": "1", "channel": "auth.user_authenticated"}

    @property
    def payload(self):
        return json.dumps(
            {
                "timestamp": "2019-08-10 08:27:13.451285+00",
                "operation": "INSERT",
                "schema": "public",
                "table": "event_outbox",
                "data": self.data,
            }
        )


class MockCursor(object):
    def __init__(
        self,
        fetchall_result: list = None,
        durable: bool = False,
        raises: Exception = None,
    ):
        self.fetchall_result = [] if not fetchall_result else fetchall_result
        self.durable = durable
        self.raises = raises

    async def execute(self, *args, **kwargs):
        if self.raises:
            raise self.raises
        return ""

    @property
    def rowcount(self):
        return len(self.fetchall_result)

    async def fetchone(self):
        return ({"test": "test"}, self.durable)

    async def __aiter__(self):
        for r in self.fetchall_result:
            yield r


class Notifies(object):
    def __init__(self):
        self.message = MockNotify()

    async def set_message(self, message):
        self.message = message

    async def get(self, *args, **kwargs):
        return self.message


class MockConnection(object):
    notifies = Notifies()

    def __init__(self):
        self.fetchall_result = None
        self.durable = False
        self.raises = None

    def set_fetchall(self, result: list):
        self.fetchall_result = result

    def set_durable(self, durable: bool):
        self.durable = durable

    def set_raises(self, exception):
        self.raises = exception

    @asynccontextmanager
    async def cursor(self):
        yield MockCursor(self.fetchall_result, durable=self.durable, raises=self.raises)


# mocking a aiopg pool
class _PoolContextManager(_ContextManager):
    async def __aexit__(self, exc_type, exc, tb):
        pass


async def _create_pool():
    return MockPool()


def create_pool(*args, **kwargs):
    coro = _create_pool()
    return _PoolContextManager(coro)


class MockPool(object):
    conn = MockConnection()

    @asynccontextmanager
    async def acquire(self, *args, **kwargs):
        yield self.conn


class TestProducer(BaseAsyncTestCase):
    @asynctest.patch("flask_microservice_commons.producer.listen")
    @asynctest.patch("flask_microservice_commons.producer.dispatch_messages")
    @asynctest.patch("flask_microservice_commons.producer.collect_pending")
    @mock.patch("aiopg.create_pool", new=create_pool)
    def test_producer_start(self, mock_listener, mock_dispatcher, mock_collector):
        """Ensure producer start works as expected"""
        nc = AioMock(NATS())
        sc = AioMock(STAN())
        self.loop.run_until_complete(producer.producer(nc, sc))
        mock_listener.assert_awaited_once()
        mock_dispatcher.assert_awaited_once()
        mock_collector.assert_awaited_once()

    async def test_listen(self):
        """Ensure database listen queue works as expected"""
        queue = asyncio.Queue()
        async with create_pool() as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    mp = MockPool()
                    await producer.listen(mp, queue, run_once=True)
                    self.assertEqual(queue.qsize(), 1)

    async def test_listen_malformed_payload(self):
        """Ensure listen function ignores malformed payload and does not write to the queue"""
        queue = asyncio.Queue()

        mp = MockPool()
        mn = MockNotify()
        mn.data = "junk"
        await mp.conn.notifies.set_message(mn)

        async with create_pool() as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    with self.assertLogs(level="WARNING") as cm:
                        await producer.listen(mp, queue, run_once=True)
                        self.assertEqual(queue.qsize(), 0)
                        self.assertEqual(len(cm.output), 1)
                        self.assertIn(
                            "[worker.producer] Data from DB message malformed",
                            cm.output[0],
                        )

    async def test_listen_invalid_message(self):
        """Ensure we can handle malformed messages coming from the database"""
        queue = asyncio.Queue()

        mp = MockPool()
        await mp.conn.notifies.set_message("junk!")

        async with create_pool() as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    with self.assertLogs(level="WARNING") as cm:
                        await producer.listen(mp, queue, run_once=True)
                        self.assertEqual(len(cm.output), 1)
                        self.assertIn(
                            "[worker.producer] Response from LISTEN cannot be parsed",
                            cm.output[0],
                        )
                        self.assertEqual(queue.qsize(), 0)

    async def test_dispatch_message(self):
        """Ensure dispatch_messages() behaves correctly"""
        queue = asyncio.Queue()
        mp = MockPool()
        msg = producer.Message(
            id="test", channel="test", payload=json.dumps({"test": "test"}).encode()
        )
        nc = NATS()
        sc = STAN()
        await queue.put(msg)
        await producer.dispatch_messages(mp, nc, sc, queue, run_once=True)
        self.assertEqual(queue.qsize(), 0)

    async def test_publish(self):
        """Ensure a durable message is sent through NATS streaming"""
        mp = MockPool()
        mp.conn.set_fetchall([1])
        msg = producer.Message(
            id="test", channel="test", payload=json.dumps({"test": "test"}).encode()
        )
        nc = AioMock(NATS)
        sc = STAN()
        await producer.publish_message(nc=nc, sc=sc, pool=mp, message=msg)
        nc.publish.assert_called_once()

    async def test_publish_durable(self):
        """Ensure a durable message is sent through NATS streaming"""
        mp = MockPool()
        mp.conn.set_fetchall([1])
        mp.conn.set_durable(True)
        msg = producer.Message(
            id="test", channel="test", payload=json.dumps({"test": "test"}).encode()
        )
        nc = AioMock(NATS())
        sc = AioMock(STAN())
        await producer.publish_message(nc=nc, sc=sc, pool=mp, message=msg)
        sc.publish.assert_called_once()

    async def test_publish_sqlerror(self):
        """Ensure a database error is detected and handled properly"""
        from sqlalchemy.exc import SQLAlchemyError

        mp = MockPool()
        mp.conn.set_raises(SQLAlchemyError)
        mp.conn.set_fetchall([1])
        msg = producer.Message(
            id="test", channel="test", payload=json.dumps({"test": "test"}).encode()
        )
        nc = AioMock(NATS())
        sc = AioMock(STAN())
        with self.assertRaises(SQLAlchemyError):
            await producer.publish_message(nc=nc, sc=sc, pool=mp, message=msg)


class TestCollectPending(BaseAsyncTestCase):
    @mock.patch("asyncio.sleep", new=AsyncMock())
    async def test_collect_pending(self):
        """Ensure collect_pending() behaves as expected"""
        pool = MockPool()
        test_data = [
            [1, "test", {}],
            [2, "test2", {}],
            [3, "kill", {"kill_process": True}],
        ]
        pool.conn.set_fetchall(test_data)
        nc = NATS()
        sc = STAN()
        with mock.patch(
            "flask_microservice_commons.producer.publish_message", new=AsyncMock()
        ):
            await collect_pending(nc, sc, pool)
            producer.publish_message.mock.assert_called()
            self.assertEqual(producer.publish_message.mock.call_count, 3)
