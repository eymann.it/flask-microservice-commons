from flask import Flask
from flask_testing import TestCase
from flask_sqlalchemy import SQLAlchemy
from flask_microservice_commons import Commons, db


class ModelTests(TestCase):
    def create_app(self):
        app = Flask(__name__)
        app.debug = True
        app.env = "testing"
        app.testing = True
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///"
        app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
        db.init_app(app)
        Commons(app, database=db)
        return app

    def setUp(self) -> None:
        from flask_microservice_commons.models import EventOutbox

        db.create_all()

    def tearDown(self) -> None:
        from flask_microservice_commons.models import EventOutbox

        db.drop_all()

    def test_create_event(self):
        from flask_microservice_commons.models import create_event, EventOutbox

        event_payload = {"test": "test"}
        db.session.flush()
        create_event(channel="test", message=event_payload, durable=False)
        db.session.commit()

        query = EventOutbox.query.all()
        self.assertEqual(len(query), 1)
        self.assertDictEqual(query[0].message, event_payload)
        self.assertEqual(query[0].channel, "test")
        self.assertFalse(query[0].durable)
        self.assertEqual(query[0].status, "pending")
