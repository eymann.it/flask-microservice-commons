import asyncio
import asynctest
import logging
from unittest import mock
import unittest
import json

from tests.base import AsyncMock, BaseAsyncTestCase
from flask_microservice_commons.worker import consumer


async def fake_result_handler(message):
    logging.info(f"fake_result_handler called with {message}")
    return {"result": "ok"}


def fake_sync_result_handler(message):
    logging.info(f"fake_sync_result_handler called with {message}")
    return {"result": "ok"}


class NCMessage(object):
    class Sub:
        def __init__(self, subject):
            self.subject = subject

    def __init__(self, payload: dict):
        self.data = json.dumps(payload).encode()
        self.subject = "test"
        self.sub = self.Sub("test")


class NATS:
    """fake nats"""

    def __init__(self, message_payload: dict = None):
        self.message = None if not message_payload else NCMessage(message_payload)

    async def subscribe(self, channel: str, queue: str, cb):
        await cb(self.message)


class STAN(NATS):
    pass


class TestConsumer(BaseAsyncTestCase):
    def setUp(self) -> None:
        self.app.config["NATS_QUEUE_GROUP"] = "test"
        self.app.config["ASYNC_TASKS"] = (
            (
                "test",
                {
                    "callback": "tests.test_consumer.fake_result_handler",
                    "durable": False,
                },
            ),
            (
                "test_import_error",
                {"callback": "tests.test_consumer.does_not_exist", "durable": False},
            ),
            ("test_import_error_2", {"callback": "no_way.i_exist", "durable": False}),
        )

    def test_consumer(self):
        """Ensure the consume() function works as expected"""
        nc = NATS({"id": "test"})
        sc = asynctest.Mock(STAN())
        with mock.patch(
            "flask_microservice_commons.consumer.handle_message", new=AsyncMock()
        ) as mock_handle_message:
            self.loop.run_until_complete(consumer.consume(nc=nc, sc=sc, run_once=True))
            mock_handle_message.mock.assert_called_once()
            mock_handle_message.mock.assert_called_with(
                "test", {"id": "test"}, s_msg=None, sc=unittest.mock.ANY
            )

    async def test_handle_message(self):
        msg = {"test": "test"}
        channel = "test"
        with self.assertLogs(level="INFO") as cm:
            await consumer.handle_message(channel, msg)
            self.assertIn(
                "fake_result_handler called with {'test': 'test'}", cm.output[0]
            )

    async def test_synchronous_handler(self):
        """Ensure a sycnhronous handler is called"""
        self.app.config["ASYNC_TASKS"] = (
            (
                "test_sync",
                {
                    "callback": "tests.test_consumer.fake_sync_result_handler",
                    "durable": False,
                },
            ),
        )
        msg = {"test": "test"}
        channel = "test_sync"

        class FakeLoop(object):
            pass

        fake_loop = FakeLoop()
        fake_loop.run_in_executor = asynctest.CoroutineMock()

        with mock.patch(
            "flask_microservice_commons.consumer.asyncio.get_running_loop",
            new=mock.Mock(return_value=fake_loop),
        ) as loop:
            with mock.patch(
                "flask_microservice_commons.consumer.asyncio.ensure_future"
            ) as mock_ensure:
                await consumer.handle_message(channel, msg)
                mock_ensure.assert_called_once()

    async def test_handle_message_wrong_mapping(self):
        msg = {"test": "test"}
        channel = "test_import_error"
        with self.assertLogs(level="WARNING") as cm:
            await consumer.handle_message(channel, msg)
            self.assertIn(
                "[worker.consumer] No handler for channel test_import_error",
                cm.output[0],
            )

    async def test_handle_message_rubbish_mapping(self):
        msg = {"test": "test"}
        channel = "test_import_error_2"
        with self.assertLogs(level="ERROR") as cm:
            await consumer.handle_message(channel, msg)
            self.assertIn(
                f"[worker.consumer] Problem loading handler no_way.i_exist for channel {channel}",
                cm.output[0],
            )

    def test_handle_results(self):
        """Ensure handle_results behaves correctly"""
        results = [{"test": "this is a normal message"}, Exception("test")]
        with self.assertLogs(level="ERROR") as cm:
            consumer.handle_results(results)
            self.assertEqual(len(cm.output), 1)
            self.assertIn("Handling general error: test", cm.output[0])

    async def test_consume(self):
        """Ensure a NATS subscription is created"""
        self.app.config["ASYNC_TASKS"] = (
            (
                "test",
                {
                    "callback": "tests.test_consumer.fake_result_handler",
                    "durable": False,
                },
            ),
        )
        nc = asynctest.Mock(NATS())
        sc = asynctest.Mock(STAN())
        queue = asynctest.Mock(asyncio.Queue)
        queue.get.return_value = NCMessage({"id": 1, "test": "test"})
        await consumer.consume(nc=nc, sc=sc, queue=queue, run_once=True)
        nc.subscribe.assert_has_awaits(
            calls=[asynctest.call("test", cb=asynctest.ANY, queue="test")],
            any_order=True,
        )

    async def test_consume_durable(self):
        """Ensure a NATS subscription is created"""
        self.app.config["ASYNC_TASKS"] = (
            (
                "test",
                {
                    "callback": "tests.test_consumer.fake_result_handler",
                    "durable": True,
                },
            ),
        )
        nc = asynctest.Mock(NATS())
        sc = asynctest.Mock(STAN())
        queue = asynctest.Mock(asyncio.Queue)
        queue.get.return_value = NCMessage({"id": 1, "test": "test"})
        await consumer.consume(nc=nc, sc=sc, queue=queue, run_once=True)
        sc.subscribe.assert_has_awaits(
            calls=[
                asynctest.call(
                    "test",
                    cb=asynctest.ANY,
                    queue="test",
                    durable_name="test",
                    manual_acks=True,
                )
            ],
            any_order=True,
        )
