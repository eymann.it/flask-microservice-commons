import asynctest
from unittest import mock
from flask_caching import Cache

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from flask_microservice_commons import Commons


def AsyncMock(*args, **kwargs):
    m = mock.MagicMock(*args, **kwargs)

    async def mock_coro(*args, **kwargs):
        return m(*args, **kwargs)

    mock_coro.mock = m
    return mock_coro


class BaseAsyncTestCase(asynctest.TestCase):
    def create_app(self):
        app = Flask(__name__)
        app.debug = True
        app.env = "testing"
        app.testing = True
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///"
        self.cache = Cache(app, config={"CACHE_TYPE": "simple"})
        Commons(app, cache=self.cache)

        return app

    def _pre_setup(self):
        self.app = self.create_app()
        self._ctx = self.app.test_request_context()
        self._ctx.push()

    def _post_teardown(self):
        if getattr(self, "_ctx", None) is not None:
            self._ctx.pop()
            del self._ctx

        if getattr(self, "app", None) is not None:
            del self.app

    def __call__(self, result=None):
        """
        Does the required setup, doing it here
        means you don't have to call super.setUp
        in subclasses.
        """
        try:
            self._pre_setup()
            super(BaseAsyncTestCase, self).__call__(result)
        finally:
            self._post_teardown()
