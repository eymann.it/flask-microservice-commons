from flask import Flask
from flask_testing import TestCase
from flask_microservice_commons import Commons, ConfigurationError


class TestClientID(TestCase):
    def create_app(self):
        commons = Commons()
        app = Flask(__name__)
        app.config["NATS_QUEUE_GROUP"] = "producer"
        commons.init_app(app)
        return app

    def test_nats_client_id(self):
        self.assertRegex(self.app.config["STAN_CLIENT_ID"], r"producer\-\w{5}")


async def async_job(*args):
    pass


class TestCronjobSpecs(TestCase):
    def create_app(self):
        app = Flask(__name__)
        return app

    def test_no_jobs(self):
        commons = Commons()
        commons.init_app(self.app)
        self.assertListEqual(
            list(self.app.extensions["microservice_commons"].cronjobs), []
        )

    def test_regular_job(self):
        self.app.config["CRONJOBS"] = [
            ["* * * * * */3", ["tests.test_config.async_job", ["test"]]]
        ]
        e = {"spec": "* * * * * */3", "func": async_job, "args": ("test",)}
        commons = Commons()
        commons.init_app(self.app)
        s = next(self.app.extensions["microservice_commons"].cronjobs)
        self.assertTrue(isinstance(s, dict))
        self.assertListEqual(list(s.keys()), list(e.keys()))
        self.assertEqual(s["func"].__name__, async_job.__name__)
        self.assertTupleEqual(s["args"], ("test",))

    def test_regular_job_no_args(self):
        self.app.config["CRONJOBS"] = [
            ["* * * * * */3", ["tests.test_config.async_job"]]
        ]
        e = {"spec": "* * * * * */3", "func": async_job, "args": ()}
        commons = Commons()
        commons.init_app(self.app)
        s = next(self.app.extensions["microservice_commons"].cronjobs)
        self.assertTrue(isinstance(s, dict))
        self.assertListEqual(list(s.keys()), list(e.keys()))
        self.assertEqual(s["args"], ())

    def test_invalid_spec_cronspec(self):
        self.app.config["CRONJOBS"] = [
            ["junk", ["tests.test_config.async_job", ["test"]]]
        ]
        e = {"spec": "* * * * * */3", "func": async_job, "args": ("test",)}
        commons = Commons()
        commons.init_app(self.app)
        with self.assertRaises(ConfigurationError):
            s = next(self.app.extensions["microservice_commons"].cronjobs)

    def test_invalid_def_import(self):
        self.app.config["CRONJOBS"] = [
            ["* * * * * */3", ["module.cannot.be.found", ["test"]]]
        ]
        e = {"spec": "* * * * * */3", "func": async_job, "args": ("test",)}
        commons = Commons()
        commons.init_app(self.app)
        with self.assertRaises(ConfigurationError):
            s = next(self.app.extensions["microservice_commons"].cronjobs)


class TestTaskDefinitions(TestCase):
    PRESET = {
        "auth.session_revoked": {
            "callback": "flask_microservice_commons.tasks.handle_session_revoked",
            "durable": False,
            "queue": None,
        },
        "auth.jwt_public_keys": {
            "callback": "flask_microservice_commons.tasks.handle_jwt_public_keys",
            "durable": False,
            "queue": None,
        },
    }

    def create_app(self):
        app = Flask(__name__)
        return app

    def test_task_definition_preset(self):
        commons = Commons()
        commons.init_app(self.app)
        expected_value = self.PRESET
        self.assertDictEqual(
            self.app.extensions["microservice_commons"].tasks, expected_value
        )

    def test_task_definition_as_dict(self):
        my_def = {
            "test": {
                "callback": "flask_microservice_commons.tasks.fake_result_handler",
                "durable": False,
                "queue": None,
            }
        }
        self.app.config["ASYNC_TASKS"] = [["test", my_def["test"]]]
        expected_value = {**my_def, **self.PRESET}
        commons = Commons()
        commons.init_app(self.app)
        self.assertDictEqual(
            self.app.extensions["microservice_commons"].tasks, expected_value
        )

    def test_invalid_task_definition_1(self):
        self.app.config["ASYNC_TASKS"] = "just a string"
        commons = Commons()
        with self.assertRaises(ConfigurationError):
            commons.init_app(self.app)

    def test_invalid_task_definition_2(self):
        self.app.config["ASYNC_TASKS"] = ["just a string"]
        commons = Commons()
        with self.assertRaises(ConfigurationError):
            commons.init_app(self.app)
