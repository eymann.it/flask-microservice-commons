import unittest
from connexion.exceptions import OAuthScopeProblem

from flask_microservice_commons.decorators import role_required


@role_required("admin")
def a(user):
    return True


class TestRoleRequiredDecorator(unittest.TestCase):
    def test_normal_operation(self):
        arg_user = {"roles": ["admin"]}
        self.assertTrue(a(arg_user, {}))

    def test_denied_raises_exception(self):
        arg_user = {"roles": ["user"]}
        with self.assertRaises(OAuthScopeProblem):
            a(arg_user, {})

    def test_no_rules_key_raises_exception(self):
        with self.assertRaises(OAuthScopeProblem):
            a({}, {})
