import time
import uuid
from unittest import mock
from tests.base import BaseAsyncTestCase
from flask_microservice_commons.tasks import (
    handle_session_revoked,
    handle_jwt_public_keys,
)


class TestTokenRevoked(BaseAsyncTestCase):
    async def test_writes_cache(self):
        """Ensure cache entry is created upon receiving a session_revoked signal"""
        message = {
            "id": str(uuid.uuid4()),
            "user_guid": str(uuid.uuid4()),
            "timestamp": int(time.time()),
        }
        await handle_session_revoked(message)
        ckey = "revoked_user_{}".format(message["user_guid"])
        self.assertIsNotNone(self.cache.get(ckey))
        timeout_at = message["timestamp"] + self.app.config.get("JWT_LIFETIME_SECONDS")
        self.assertEqual(self.cache.get(ckey), timeout_at)


class TestJWTPublicKeys(BaseAsyncTestCase):
    async def test_handle_jwt_public_keys(self):
        """Ensure handle_jwt_public_keys works as expected"""
        message = {
            "key_id": "12345",
            "timestamp": int(time.time()),
            "public_key": "test",
        }
        await handle_jwt_public_keys(message)
        self.assertIsNotNone(self.cache.get(f"jwt_public_key_12345"))
