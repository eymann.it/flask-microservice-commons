"""
Flask-Microservice-Commons
-------------

Useful integration tools for microservice architectures
"""
from setuptools import setup


setup(
    name="Flask-Microservice-Commons",
    version="0.1.6",
    url="http://gitlab.com/eymann.it/flask-microservice-commons/",
    entry_points={
        "flask.commands": [
            "request-publickeys=flask_microservice_commons.cli:request_publickeys",
            "worker=flask_microservice_commons.cli:worker",
        ]
    },
    license="BSD",
    author="Peter Eymann",
    author_email="info@eymann.it",
    description="Integration tools for microservice architectures",
    long_description=__doc__,
    packages=["flask_microservice_commons"],
    zip_safe=False,
    include_package_data=True,
    platforms="any",
    install_requires=[
        "Flask==1.1.1",
        "asyncio-nats-client==0.9.2",
        "asyncio-nats-streaming==0.4.0",
        "aiopg==0.16.0",
        "attrs==19.1.0",
        "pyjwt[crypto]==1.7.1",
        "connexion==2.3.0",
        "Flask-SQLAlchemy==2.4.0",
        "aiocron==1.3",
        "Flask-Caching==1.7.2",
    ],
    tests_require=["asynctest==0.13.0", "Flask-Testing==0.7.1", "coverage==4.5.4"],
    test_suite="tests",
    classifiers=[
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
)
