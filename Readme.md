[![pipeline status](https://gitlab.com/eymann.it/flask-microservice-commons/badges/master/pipeline.svg)](https://gitlab.com/eymann.it/flask-microservice-commons/commits/master)
[![coverage report](https://gitlab.com/eymann.it/flask-microservice-commons/badges/master/coverage.svg)](https://gitlab.com/eymann.it/flask-microservice-commons/commits/master)
# Flask Microservice Commons

A collection of useful tools to create event sourced microservices with Flask.  
Offers a solution for transactional, asynchronous messaging using nats, nats-streaming
and PostgresSQL. 

## Install
```
pip install https://gitlab.com/eymann-it/flask-microservice-commons.git
```

## Start the worker

```
from flask import Flask
from flask_caching import Cache

from flask_microservice_commons import Commons

app = Flask(__name__)
cache = Cache(app)

commons = Commons(app, cache)

worker.worker()
```
 
